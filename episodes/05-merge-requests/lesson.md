<!---
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden - Rossendorf (HZDR) <hifis-info@hzdr.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Merge Requests and Reviews

> ## Learning Goals
> * What are merge requests?
> * How to create merge requests?
> * How to make changes via WebIDE or from a cloned repository?
> * How to resolve merge requests?
> * How to review changes?
>   * Annotate lines
>   * Make suggestions

---

## What are Merge Requests
* **Note:** This feature has different names on different platforms:
  * In GitLab: _Merge Requests_
  * In GitHub: _Pull Requests_

* _Merge Requests_ (short MR) visualize the development process on a git branch, including:
  * Commits
  * Conversations
  * Assignees
  * Relations to issues and other MRs
* They form the basis for _reviews_ 

## Creating Merge Requests
* MRs can be created based on
  * Existing issues
    * Will create a new branch with an automatically generated name
  * Branches pushed via git
* After creation, a MRs' title, description and merge options can be set anytime
* Like issues, MRs can have labels and assignees

> ## Do Together
> * Create a new issue
> * Create a merge request for this issue
> * Edit the merge request
> * Explain the squash-commit option

## Making Changes
* To add changes to the MR
  * Push commits to the MR branch via git
  * Use the WebIDE

> ## Do Together
> * Use the WebIDE to change files in the previously created MR

* When satisfied with the changes, request a _review_

## Interaction
The interaction principles (commenting, adding files, referencing) are similar to the ones already seen for issues.

## Reviews
Reviews are an important part of the development process that often gets overlooked or cut short due to time or budget constraints.
They are however an important tool to keep the project's overall quality high and should thus also be endorsed for their long-term effect.
Most commonly, reviews are to be found in software development, but variations can be found in engineering, schience, literature (lectorate)…

* People make mistakes 
* When working on the same task for a long time, poeple tend tunnel vision
* Reviews allow to catch shortcomings and endorse style guides and similar agreements
* Each merge request should be reviewed, even trivial ones
  * No-one should review their own merge request
* Everyone on the team reviews, everyone gets reviewed
* Finding nothing that needs to be changed is a valid review result

### Best Practises

```
Sticking to a good review practise makes the difference between reviews being a dreaded nuissance or a valueable team experience.
```

* Reviewing requires focussed working, prefer more but shorter review sessions (at most 1 hour, or only a few hundred lines of code/text)
  * Keep merge requests small (and therefor issues) on-point
* Automate whatever possible (more on that in the CI episode)
* Be professional
  * Give constructive feedback 
  * Do not condescent 
  * Talk about the code, not the author 
  * Assume best intentions 
  * Regard a review as an opportunity to teach and learn.
* Also highlight good and clean solutions to encourage continuing best practises.
* High quality documentation can help the reviewer to understand the code better and faster, reducing the effort required
* A review check-list can be helpful

### Further Reading
* [Philpp Hauer's Blog Post on Code Reviews](https://phauer.com/2018/code-review-guidelines/)

### During Review

* Comments can be put in line-wise
  * Comments can contain suggestions that can be directly applied by the original author

> ## Do Together
> * Add a comment to a line in the review
> * Make a suggestion
> * Apply a suggestion

## After the Review

* Once the review is complete, the reviewer
  * Informs the author about the completion
  * Approves the changes if no problems were found
  * Merges the changes, if all reviewers have approved the changes

---

> ## Wrap-Up
>
> * Merge Requests visualize the development on a branch
> * They allow interaction such as commenting in general or on specific code lines
> * Reviews are important and doing them right is not easy
