<!---
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf <hifis-info@hzdr.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Establishing a Contribution Workflow

> ## Learning Goals
> * Motivate the agreement on shared workflows
> * Introduce an example workflow for collaboration
>
> **Estimated time:** 15 minutes

## Why bother?
* Agreeing on how to handle day-to-day operations increases productivity and
  reduces the potential for miscommunication
  * Reduces onboarding time for new project members
  * Reduces setup time for new projects
* Workflows that deal with similar types of projects should be similar
  * There is no one-workflow-to-rule-them-all
  * Adapt established workflows to team- or project-specific needs

> ## Question to the Participants
> **Estimated time:** 5 minutes
> 
> * In your experience, how does one contribute changes to a team project?
>   * Which steps had to be taken?
>   * How did the communication with other team members work?
> * What worked well with these approaches, what dd not work well?

## An Example Contribution Workflow

```
A contribution workflow regulates how changes to the files of the project
get planned, agreed upon, executed and verified.
```
* The contribution workflow should be represented in the `Contributing` file

In this course, a workflow will be presented that is especially suited for
GitLab (or GitHub) and can be applied for many types of projects.
The following is an overview, the details will be subject of the following 
episodes.

![Visual representation of the workflow](resources/GitLab-Workflow.svg)

1. Open an issue
   * Detail the intended changes
   * Notify potential stakeholders
2. Discuss the details 
   * Clarify open questions.
   * When working face-to-face this can be kept to a minimum. Write down the 
     decisions that were made in meetings or attach a protocol.
3. Open a new _merge request_
   * This signals that someone is working on it
   * A merge request automatically generates a _git branch_ to be used.
4. Make iterative changes
   * Add commits to the branch
   * When satisfied request a review of the changes made
   * Adapt according to the reviews
5. Once the reviewers approved the changes, merge the working branch into the master branch
   of the project (or any other target branch for that matter)

### Structuring Interaction

When practicing short development cycles it can happen that the preparation phase overlaps the implementation phase
(i.e. someone starts working on the merge request before all details are clarified).
With the possibility to add comments to the issue as well as the merge request (see the respective episodes), it can be difficult to decide where a comment belongs to.

As a rule of thumb:
* Discuss the details of the problem and solution approaches in the issue
* Discuss the actual presented solution in the merge request

> ## Wrap-up
> * Establishing workflows is a good idea 
>  * It introduces a well-documented, standardized and structured way of collaboration in your project 
>  * Tailored workflows fit your project context, your project's needs and the habits and practices of your collaborators
> * Got to know one example workflow for contributing in GitLab

