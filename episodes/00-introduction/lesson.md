<!---
SPDX-FileCopyrightText: 2020 HIFIS <hifis-info@hzdr.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Introduction
> **Estimated time:** 10 minutes

---

## What is _GitLab_

* _GitLab_ is a project management platform built on top of the version control system _Git_.
* Around the core of the version control system, features are added to aid with working in teams, managing projects and automating tasks
* It is purely web-based, the user is not forced to install _git_ to use it
  * Depending on the project, it can be practical though
* The software is open source and any company/ institution can run their own _instance_
  * The "default" instance is [gitlab.com]
  * The instance of the HZDR for example is [gitlab.hzdr.de](https://gitlab.hzdr.de)
  * Other Helmholtz - centers similarly run their own instances
* Business model of GitLab revolves around premium features offered on [gitlab.com] that usually move towards the free version after some time

## Similar Platforms in the Wild
 
* GitHub
  * Launched 2008, now owned by Microsoft
* Bitbucket
  * Launced 2008 by Atlassian
* SourceForge
  * For open source projects only

> **Side Note:** Since all of these platforms have their headquarters in the US,trade restrictions may apply when the user is located in 
> * Crimea 
> * Cuba
> * Iran 
> * North Korea
> * Sudan
> * Syria

## General Notes

* Nearly every UI element in GitLab is either a link and/or has some tooltips associated
  * Also many features have a direct link to the associated documentation
  * **Hint:** In most browsers, middle-mouse click or `Ctrl` click is used for opening a link in a new tab. This is a feature that comes in very handy when learning about GitLab
* On [gitlab.com], public repositories have acces to some of the premium features, so the UI may look a bit different

## Setting up SSH keys

Not all GitLab instances allow authentication with passwords for pushing and pulling, since SSH-Keys are a much safer option.
SSH keys serve as authentication for users (the same idea as e.g. passports or real-life keys).
They come in pairs for each combination of users and/or services:
* A _public_ key that is given out to the service (GitLab in this case) who wants to communicate with the user
* A _private_ key that the user keeps secret

> Sharing your public keys with other people or services is completely fine and intended.
> Do however **not** share or re-use your private keys! This is a tremendous security risk!

This example is modified for use with the Helmholtz GitLab.
See also the full [documentation on `gitlab.com`](https://docs.gitlab.com/ee/ssh/)

### Step-by-step
1. Open your command line interface (e.g. GitBash or the shell)
2. Run the command 
```bash
ssh-keygen -t ed25519 -C "<comment>"
```
where `<comment>` usually indicates the use and the associated email.
Note that this comment is put into the public key and is not secret.
3. When prompted for the directory and file name, note down the directory you save it in (by default `~/.ssh/`) and give it a more helpful name (e.g. `helmholtz_gitlab_ed25519`)
4. Provide a strong password and store it safely. There is no recovery option!
5. Display the newly generated public key via `cat ~/.ssh/helmholtz_gitlab_ed25519.pub`. (Note the file ending!)
6. Select and copy the output (Remember: in a GitBash or shell copying is done with `Ctrl + Shift + c`)
7. Sign in to GitLab, then:
  1. Click your avatar in the top right corner
  2. Click _Settings_
  3. In the left sidebar click _SSH Keys_
  4. Paste the key you copied into the _Text_ box. Make sure it starts with `ssh-ed25519` and ends with the comment you gave earlier. Otherwise you may have missed something while copying
  5. In the _Title_ box enter a name to distinguish the key (in case there are multiple) e.g. `Work <Date of creation>`
  6. Click _Add key_

To automatically use a given key for pushing to/pulling from GitLab create/edit a file `~/.ssh/config` and modify the following example:
```
# Configuration for Helmholtz Gitlab
Host gitlab.hzdr.de
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/helmholtz_gitlab_ed25519
```

[gitlab.com]: https://gitlab.com
