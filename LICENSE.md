# License

Copyright © 2020 Helmholtz-Zentrum Dresden - Rossendorf (HZDR)

This work is licensed under a [CC-BY-4.0](LICENSES/CC-BY-4.0.txt) license.

Please see the individual files for more accurate information.

> **Hint:** We provide the copyright and license information in accordance with the [REUSE Specification 3.0](https://reuse.software/spec/).
